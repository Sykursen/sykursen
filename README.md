# Hello there 👋

[![Website badge](https://img.shields.io/website?down_color=red&up_color=green&url=https%3A%2F%2Fhello.guillaume.engineer)](https://hello.guillaume.engineer)
[![Twitter badge](https://img.shields.io/twitter/follow/Sykursen?style=social)](https://twitter.com/Sykursen)
[![Twitter badge](https://img.shields.io/badge/LinkedIn-0077B5?style=social&logo=linkedin)](https://www.linkedin.com/in/guillaume-assier)

I'm Guillaume 🦎

- Cybersecurity architect 🖌️ 
- Hopledge labs founder 📚 
- 50 nuances d'octets co-founder 🎙️
- Crowdsec ambassador 🦙
- Cyberdefense student at ENSIBS 🧭

Also present on [Github](https://github.com/Sykursen) 
